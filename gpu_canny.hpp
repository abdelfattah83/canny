#ifndef GPU_CANNY_HPP
#define GPU_CANNY_HPP

class CannyGPU {
    int device_id;
    int img_height;
    int img_width;
    int kernel_sz;
    int gauss_sigma;
    double highThr;
    double lowThr;

    double *img, *dev_img;
    double *grad_mag, *dev_grad_mag;
	double *Ix, *Iy, *dev_Ix, *dev_Iy;
    double *subpix_edge_pts_strong,      *dev_subpix_edge_pts_strong;
    double *subpix_edge_pts_weak,        *dev_subpix_edge_pts_weak;
    double *subpix_edge_pts_weak2strong, *dev_subpix_edge_pts_weak2strong;
    double *subpix_edge_pts_final,       *dev_subpix_edge_pts_final;
    double *subpix_idx_map,              *dev_subpix_idx_map;
    double *dev_maskx, *dev_masky;

  public:

    int num_of_candidate_strong;
    int num_of_candidate_weak;

    int num_of_final_edge_pts;
    int edge_pt_sz;

    CannyGPU(int, int, int, int, int, double);
    ~CannyGPU();

    void preprocessing(std::ifstream& scan_infile);
    void convolve_img();
    void non_maximum_suppresion();
    void edge_hysteresis();

    void read_array_from_file(std::string filename, double **rd_data, int first_dim, int second_dim);
    void write_array_to_file(std::string filename, double *wr_data, int first_dim, int second_dim);
};



#endif    // GPU_CANNY_HPP