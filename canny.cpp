#include <cmath>
#include <math.h>
#include <fstream>
#include <iostream>
#include <string.h>
#include <vector>

#include "canny.hpp"
#include "indices.hpp"

// ==================================== Constructor ===================================
// Define parameters used by functions in the class and allocate 2d arrays dynamically
// ====================================================================================
CannyCPU::CannyCPU (int H, int W, int kernel_size, int sigma, double high_thr) {
    img_height = H;
    img_width = W;

    kernel_sz = kernel_size;
    gauss_sigma = sigma;
    highThr = high_thr;
    lowThr = 0.4 * highThr;

    grad_mag       = new double[img_height*img_width];
    img            = new double[img_height*img_width];
    Ix             = new double[img_height*img_width];
    Iy             = new double[img_height*img_width];
    subpix_idx_map = new double[img_height*img_width];

    edge_pt_sz = (int)(img_height*img_width*0.5);

    subpix_edge_pts_strong      = new double[edge_pt_sz * 6];
    subpix_edge_pts_weak        = new double[edge_pt_sz * 6];
    subpix_edge_pts_weak2strong = new double[edge_pt_sz * 6];
    subpix_edge_pts_final       = new double[edge_pt_sz * 6];
}

// ========================= preprocessing ==========================
// Initialize 2d arrays
// ==================================================================
void CannyCPU::preprocessing(std::ifstream& scan_infile) {
    int r_idx = 0, col_idx = 0;
    int temp;
    for (int i = 0; i < img_height; i++) {
        for (int j = 0; j < img_width; j++) {
            if (j == 0) {
                temp = (int)scan_infile.get();
                continue;
            }
            img(r_idx, col_idx) = (int)scan_infile.get();
            Ix(r_idx, col_idx) = 0;
            Iy(r_idx, col_idx) = 0;
            subpix_idx_map(r_idx, col_idx) = -1;
            col_idx++;
        }
        col_idx = 0;
        r_idx++;
    }

    for (int i = 0; i < edge_pt_sz; i++) {
        for (int j = 0; j < 6; j++) {
            subpix_edge_pts_strong(i, j) = 0;
            subpix_edge_pts_weak(i, j)   = 0;
            subpix_edge_pts_final(i, j)  = 0;
        }
    }

    //write_array_to_file("input_img_data.txt", img, img_height, img_width);
}

// ================================== convolve Image ==========================================
// We don't pad the input image here as we assume the padded area are all zeros.
// (1) Start by filling in the mask values using the Gausian 1st derivative.
// (2) Do a scanning convolution on the input img matrix. This gives us the Δy and Δx matrices
// (3) Take the sqrt of the sum of Δy^2 and Δx^2 to find the magnitude.
// ============================================================================================
void CannyCPU::convolve_img()
{
	const int cent = (kernel_sz-1)/2;
	double maskx[kernel_sz * kernel_sz], masky[kernel_sz * kernel_sz];
	double Gx, Gy, G;
	const double PI = 3.14159265358979323846;

	// -- Use the Gaussian 1st derivative formula to fill in the mask values --
	for (int p = -cent; p <= cent; p++) {
		for (int q = -cent; q <= cent; q++) {
			// -- Gaussian derivative in x --
			Gx = (-(q)*std::exp(-(q*q)/(2*gauss_sigma*gauss_sigma)))/(std::sqrt(2*PI)*gauss_sigma*gauss_sigma*gauss_sigma);
			G = std::exp(-(p*p)/(2*gauss_sigma*gauss_sigma))/(std::sqrt(2*PI)*gauss_sigma);
			maskx(p+cent,q+cent) = Gx * G;


			// -- Gaussian derivative in y --
			Gy = (-(p)*std::exp(-(p*p)/(2*gauss_sigma*gauss_sigma)))/(std::sqrt(2*PI)*gauss_sigma*gauss_sigma*gauss_sigma);
			G = std::exp(-(q*q)/(2*gauss_sigma*gauss_sigma))/(std::sqrt(2*PI)*gauss_sigma);
			masky(p+cent,q+cent) = Gy * G;
		}
	}

	// -- do convolution and compute gradient magnitude --
	for (int i = 0; i < img_height; i++) {
		for (int j = 0; j < img_width; j++) {

			// loop over the kernel
			for (int p = -cent; p <= cent; p++) {
				for (int q = -cent; q <= cent; q++) {
					if ((i+p) < 0 || (j+q) < 0 || (i+p) >= img_height || (j+q) >= img_width)
						continue;

					Ix(i,j) += img(i+p, j+q) * maskx(p+cent, q+cent);
					Iy(i,j) += img(i+p, j+q) * masky(p+cent, q+cent);
				}
			}

			// -- compute magnitude --
			grad_mag(i, j) = std::sqrt( Ix(i, j) * Ix(i, j) + Iy(i, j) * Iy(i, j) );
		}
	}
}

// ======================================== Non-maximal Suppression (NMS) ============================================
// (1) Decide the quadrant the gradient belongs to by looking at the signs and size of gradients in x and y directions
// (2) Points which magnitude are greater than both it's neighbors in the direction of their gradients (slope) are
//     considered as peaks.
// (3) Find the subpixel of the edge point by fitting a parabola. This comes from:
//     R. B. Fisher and D. K. Naidu, “A comparison of algorithms for subpixel peak detection,” in Image Technology,
//     Advances in Image Processing, Multimedia and Machine Vis., Berlin, Germany:Springer, 1996, pp. 385–404.
// ====================================================================================================================
void CannyCPU::non_maximum_suppresion()
{
    double norm_dir_x, norm_dir_y;
    double slope, fp, fm;
    double coeff_A, coeff_B, coeff_C, s, s_star;
    double max_f, subpix_grad_x, subpix_grad_y;
    double candidate_edge_pt_x, candidate_edge_pt_y;
    double subpix_grad_mag;
    int edge_pt_idx_strong = 0;
    int edge_pt_idx_weak = 0;

    for (int j = 2; j < img_width - 2; j++) {
        for (int i = 2; i < img_height - 2; i++) {
            // -- ignore neglectable gradient magnitude --
            if (grad_mag(i, j) <= 2)
                continue;

            // -- ignore invalid gradient direction --
            if ((std::abs(Ix(i, j)) < 1e-6) && (std::abs(Ix(i, j)) < 1e-6))
                continue;

            // -- calculate the unit direction --
            norm_dir_x = Ix(i,j) / grad_mag(i,j);
            norm_dir_y = Iy(i,j) / grad_mag(i,j);

            // -- find corresponding quadrant --
            if ((Ix(i,j) >= 0) && (Iy(i,j) >= 0)) {
                if (Ix(i,j) >= Iy(i,j)) {         // -- 1st quadrant --
                    slope = norm_dir_y / norm_dir_x;
                    fp = grad_mag(i, j+1) * (1-slope) + grad_mag(i+1, j+1) * slope;
                    fm = grad_mag(i, j-1) * (1-slope) + grad_mag(i-1, j-1) * slope;
                }
                else {                              // -- 2nd quadrant --
                    slope = norm_dir_x / norm_dir_y;
                    fp = grad_mag(i+1, j) * (1-slope) + grad_mag(i+1, j+1) * slope;
                    fm = grad_mag(i-1, j) * (1-slope) + grad_mag(i-1, j-1) * slope;
                }
            }
            else if ((Ix(i,j) < 0) && (Iy(i,j) >= 0)) {
                if (abs(Ix(i,j)) < Iy(i,j)) {     // -- 3rd quadrant --
                    slope = -norm_dir_x / norm_dir_y;
                    fp = grad_mag(i+1, j) * (1-slope) + grad_mag(i+1, j-1) * slope;
                    fm = grad_mag(i-1, j) * (1-slope) + grad_mag(i-1, j+1)  * slope;
                }
                else {                              // -- 4th quadrant --
                    slope = -norm_dir_y / norm_dir_x;
                    fp = grad_mag(i, j-1) * (1-slope) + grad_mag(i+1, j-1) * slope;
                    fm = grad_mag(i, j+1) * (1-slope) + grad_mag(i-1, j+1) * slope;
                }
            }
            else if ((Ix(i,j) < 0) && (Iy(i,j) < 0)) {
                if(abs(Ix(i,j)) >= abs(Iy(i,j))) {            // -- 5th quadrant --
                    slope = norm_dir_y / norm_dir_x;
                    fp = grad_mag(i, j-1) * (1-slope) + grad_mag(i-1, j-1) * slope;
                    fm = grad_mag(i, j+1) * (1-slope) + grad_mag(i+1, j+1) * slope;
                }
                else {                              // -- 6th quadrant --
                    slope = norm_dir_x / norm_dir_y;
                    fp = grad_mag(i-1, j) * (1-slope) + grad_mag(i-1, j-1) * slope;
                    fm = grad_mag(i+1, j) * (1-slope) + grad_mag(i+1, j+1) * slope;
                }
            }
            else if ((Ix(i,j) >= 0) && (Iy(i,j) < 0)) {
                if(Ix(i,j) < abs(Iy(i,j))) {      // -- 7th quadrant --
                    slope = -norm_dir_x / norm_dir_y;
                    fp = grad_mag(i-1, j) * (1-slope) + grad_mag(i-1, j+1) * slope;
                    fm = grad_mag(i+1, j) * (1-slope) + grad_mag(i+1, j-1) * slope;
                }
                else {                              // -- 8th quadrant --
                    slope = -norm_dir_y / norm_dir_x;
                    fp = grad_mag(i, j+1) * (1-slope) + grad_mag(i-1, j+1) * slope;
                    fm = grad_mag(i, j-1) * (1-slope) + grad_mag(i+1, j-1) * slope;
                }
            }

            // -- fit a parabola to find the edge subpixel location when doing max test --
            s = std::sqrt(1+slope*slope);
            if((grad_mag(i, j) >  fm && grad_mag(i, j) > fp) ||  // -- abs max --
               (grad_mag(i, j) >  fm && grad_mag(i, j) >= fp) || // -- relaxed max --
               (grad_mag(i, j) >= fm && grad_mag(i, j) >  fp)) {

                // -- fit a parabola; define coefficients --
                coeff_A = (fm+fp-2*grad_mag(i, j))/(2*s*s);
                coeff_B = (fp-fm)/(2*s);
                coeff_C = grad_mag(i, j);

                s_star = -coeff_B/(2*coeff_A); // -- location of max --
                max_f = coeff_A*s_star*s_star + coeff_B*s_star + coeff_C; // -- value of max --

                if(abs(s_star) <= std::sqrt(2)) { // -- significant max is within a pixel --

                    // -- subpixel magnitude in x and y --
                    subpix_grad_x = max_f*norm_dir_x;
                    subpix_grad_y = max_f*norm_dir_y;

                    // -- subpixel gradient magnitude --
                    subpix_grad_mag = std::sqrt(subpix_grad_x*subpix_grad_x + subpix_grad_y*subpix_grad_y);

                    // -- double thresholding: decide whether the edge point is a strong or weak point, or should be eliminated --
                    // -- Put strong points in subpix_edge_pts_strong array --
                    // -- Put weak points in subpix_edge_pts_weak array, also put weak point index in a index map (subpix_idx_map) --
                    if (subpix_grad_mag >= highThr) {   // -- strong edge point --

                        // -- store subpixel location --
                        subpix_edge_pts_strong(edge_pt_idx_strong,0) = j + s_star * norm_dir_x;
                        subpix_edge_pts_strong(edge_pt_idx_strong,1) = i + s_star * norm_dir_y;

                        // -- store subpixel orientation --
                        subpix_edge_pts_strong(edge_pt_idx_strong,2) = std::atan2(norm_dir_x, -norm_dir_y);

                        // -- subpixel gradient magnitude --
                        subpix_edge_pts_strong(edge_pt_idx_strong,3) = subpix_grad_mag;

                        // -- pixel value --
                        subpix_edge_pts_strong(edge_pt_idx_strong,4) = j;
                        subpix_edge_pts_strong(edge_pt_idx_strong,5) = i;

                        edge_pt_idx_strong++;
                    }
                    else if ((subpix_grad_mag < highThr) && (subpix_grad_mag > lowThr)) {   // -- weak edge point --

                        // -- store subpixel location --
                        subpix_edge_pts_weak(edge_pt_idx_weak,0) = j + s_star * norm_dir_x;
                        subpix_edge_pts_weak(edge_pt_idx_weak,1) = i + s_star * norm_dir_y;

                        // -- store subpixel orientation --
                        subpix_edge_pts_weak(edge_pt_idx_weak,2) = std::atan2(norm_dir_x, -norm_dir_y);

                        // -- subpixel gradient magnitude --
                        subpix_edge_pts_weak(edge_pt_idx_weak,3) = subpix_grad_mag;

                        // -- pixel value --
                        subpix_edge_pts_weak(edge_pt_idx_weak,4) = j;
                        subpix_edge_pts_weak(edge_pt_idx_weak,5) = i;

                        // -- store weak point index to subpixel index map --
                        subpix_idx_map(i, j) = edge_pt_idx_weak;
                        edge_pt_idx_weak++;
                    }
                    else {
                        continue;
                    }
                }
            }
        }
    }

    // -- number of strong and weak edge points --
    num_of_candidate_strong = edge_pt_idx_strong;
    num_of_candidate_weak = edge_pt_idx_weak;

    std::cout<<"Number of STRONG and WEAK candidate edge points: "<<num_of_candidate_strong<<", "<<num_of_candidate_weak<<std::endl;
    // -- for debug and testing --
    write_array_to_file("data_strong.txt", subpix_edge_pts_strong, edge_pt_idx_strong, 6);
    write_array_to_file("data_weak.txt", subpix_edge_pts_weak, edge_pt_idx_weak, 6);
    //write_array_to_file("data_subpix_idx_map.txt", subpix_idx_map, img_height, img_width);
}

// ======================================== Edge Hysteresis (Edge Tracking) ============================================
// Edge hysteresis decides which weak points should be turned into strong points, and which should be disappeared. This
// is done by looking at the 3x3 neighbors of a strong points, and if there is a weak point in the neigbor, make that weak
// point to a strong point. After looking at the neighbors of "ALL the strong points", including those who initially are
// strong points and those who turned from weak to strong, if there is no weak points, then edge hysteresis is finished.
// Process:
// (1) Look at each strong edge in the subpix_edge_pts_strong array; if a weak edge appears in its 3x3 neighborhood, put
//     that weak edge to the end of the subpix_edge_pts_strong array.
// (2) End the process when all edges in the subpix_edge_pts_strong array has been visited.
// ======================================================================================================================
void CannyCPU::edge_hysteresis() {
    int end_idx_strong_list = num_of_candidate_strong;
    int x_strong, y_strong, candidate_idx;
    int append_strong_list_idx = num_of_candidate_strong;

    for (int i = 0; i < end_idx_strong_list; i++) {
        // -- retrieve pixel value --
        x_strong = subpix_edge_pts_strong(i,4);
        y_strong = subpix_edge_pts_strong(i,5);

        // -- loop over the 3x3 neighbors of the strong/weak2strong point --
        for (int p = -1; p <= 1; p++) {
            for (int q = -1; q <= 1; q++) {
                // -- find the index of the neighbors from the subpix_idx_map --
                candidate_idx = subpix_idx_map(y_strong+p,x_strong+q);
                if (candidate_idx >= 0) {  // -- if it is a weak edge point --

                    // -- put that weak point to the end of the subpix_edge_pts_strong array --
                    for (int k = 0; k < 6; k++) {
                        subpix_edge_pts_strong(append_strong_list_idx,k) = subpix_edge_pts_weak(candidate_idx,k);
                    }
                    append_strong_list_idx++;

                    // -- turn off the weak2strong point from the index map --
                    subpix_idx_map(y_strong+p,x_strong+q) = -1;
                }
            }
        }
        end_idx_strong_list = append_strong_list_idx;
    }

    num_of_final_edge_pts = end_idx_strong_list;
    std::cout<<"Number of edge points  = "<<num_of_final_edge_pts<<std::endl;

    // -- write a list of edges to the file --
    write_array_to_file("data_final_output.txt", subpix_edge_pts_strong, num_of_final_edge_pts, 6);
}

// ===================================== Write data to file for debugging =======================================
// Writes a 2d dybamically allocated array to a text file for debugging
// ==============================================================================================================
void CannyCPU::write_array_to_file(std::string filename, double *wr_data, int first_dim, int second_dim)
{
#define wr_data(i, j) wr_data[(i) * second_dim + (j)]

    std::cout<<"writing data to a file "<<filename<<" ..."<<std::endl;
    std::string out_file_name = "./test_files/";
    out_file_name.append(filename);
	std::ofstream out_file;
    out_file.open(out_file_name);
    if ( !out_file.is_open() )
      std::cout<<"write data file cannot be opened!"<<std::endl;

	for (int i = 0; i < first_dim; i++) {
		for (int j = 0; j < second_dim; j++) {
			out_file << wr_data(i, j) <<"\t";
		}
		out_file << "\n";
	}

    out_file.close();
#undef wr_data
}

// ===================================== Write data to file for debugging =======================================
// Reads data for debugging
// ==============================================================================================================
void CannyCPU::read_array_from_file(std::string filename, double **rd_data, int first_dim, int second_dim) {
    std::cout<<"reading data from a file ..."<<std::endl;
    std::string in_file_name = "./test_files/";
    in_file_name.append(filename);
    std::fstream in_file;
    double data;
    int idx_x = 0, idx_y = 0;

    in_file.open(in_file_name, std::ios_base::in);
    if (!in_file) {
        std::cerr << "input read file not existed!\n";
    }
    else {
        while (in_file >> data) {
            rd_data[idx_y][idx_x] = data;
            idx_x++;
            if (idx_x == second_dim) {
                idx_x = 0;
                idx_y++;
            }
        }
    }
}

// ===================================== Destructor =======================================
// Free all the 2d dynamic arrays allocated in the constructor
// ========================================================================================
CannyCPU::~CannyCPU () {
    // free memory
    delete[] img;
    delete[] grad_mag;
    delete[] Ix;
    delete[] Iy;
    delete[] subpix_edge_pts_strong;
    delete[] subpix_edge_pts_weak;
    delete[] subpix_edge_pts_weak2strong;
    delete[] subpix_edge_pts_final;
    delete[] subpix_idx_map;
}
