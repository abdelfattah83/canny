CUDADIR ?= /usr/local/cuda
INC=-I${CUDADIR}/include
LIBDIR=-L${CUDADIR}/lib64
LIB=-lcudart

NVCCFLAGS  = -O3  -Xcompiler "-fPIC -Wall -Wno-unused-function -Wno-strict-aliasing" -std=c++11
NVCCFLAGS += -gencode arch=compute_70,code=sm_70
NVCCFLAGS += -gencode arch=compute_80,code=sm_80

canny: main.o canny.o gpu_canny.o gpu_convolve.o
	g++ ${INC} -o canny main.o canny.o gpu_canny.o gpu_convolve.o ${LIBDIR} ${LIB}

main.o: main.cpp
	g++ -c main.cpp -o main.o

canny.o: canny.cpp
	g++ -c canny.cpp -o canny.o

gpu_canny.o: gpu_canny.cpp
	g++ ${INC} -c gpu_canny.cpp -o gpu_canny.o

gpu_convolve.o: gpu_convolve.cu
	nvcc ${INC} ${NVCCFLAGS} -c gpu_convolve.cu -o gpu_convolve.o

clean:
	rm -f canny *.o
