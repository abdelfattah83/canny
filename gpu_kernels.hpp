#ifndef GPU_KERNELS_HPP
#define GPU_KERNELS_HPP

#include<stdio.h>
#include<cuda.h>
#include<cuda_runtime_api.h>

// cuda error check
#define cudacheck( a )  do { \
                            cudaError_t e = a; \
                            if(e != cudaSuccess) { \
                                printf("Error in %s: %s\n", __func__, cudaGetErrorString(e)); \
                            }\
                        } while(0)

#ifdef __cplusplus
extern "C" {
#endif

void gpu_convolve(
        int device_id,
        int h, int w, double* dev_img,
        double* dev_maskx, double* dev_masky,
        double* dev_Ix, double* dev_Iy,
        double* dev_grad_mag );

#ifdef __cplusplus
    }
#endif

#endif // GPU_KERNELS_HPP
