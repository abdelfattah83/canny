#include <cmath>
#include <fstream>
#include <iterator>
#include <iostream>
#include <string.h>
#include <vector>
#include <stdint.h>

#include "canny.hpp"
#include "gpu_canny.hpp"

int main(int argc, char **argv)
{
	// -- Exit if the input image file doesn't open --
	std::string filename(argv[1]);
	std::ifstream infile(filename, std::ios::binary);
	if (!infile.is_open())
	{
		std::cout << "File " << filename << " not found in directory." << std::endl;
		return 0;
	}

	char type[10];
	int height, width, intensity;
	// -- Storing header information and copying into the new ouput images --
	infile >> type >> width >> height >> intensity;

	// -- define parameters --
	// -- (This could be changed to argv input arguments but now let's make it fixed)
	int kernel_size = 17;
	int sigma = 2;
	double high_thr = 12;

	printf("\nRunning on the CPU\n");
	printf("==================\n");
	// -- class constructor --
	CannyCPU ced(height, width, kernel_size, sigma, high_thr);

	// -- preprocessing: array initialization --
	ced.preprocessing(infile);

	// -- convolve image with Gaussian derivative filter --
	ced.convolve_img();

	// -- do non-maximum suppression --
    ced.non_maximum_suppresion();

	// -- do edge hysteresis (edge tracking) --
    ced.edge_hysteresis();

    // go back to the beginning of the file
    infile.clear();
    infile.seekg(0);
    infile >> type >> width >> height >> intensity;


	printf("\nRunning on the GPU\n");
	printf("==================\n");
	// -- class constructor --
	CannyGPU ced_gpu(0, height, width, kernel_size, sigma, high_thr);

	// -- preprocessing: array initialization --
	ced_gpu.preprocessing(infile);

	// -- convolve image with Gaussian derivative filter --
	ced_gpu.convolve_img();

	// -- do non-maximum suppression --
    ced_gpu.non_maximum_suppresion();

	// -- do edge hysteresis (edge tracking) --
    ced_gpu.edge_hysteresis();

    // close file
    infile.close();

	return 0;
}
