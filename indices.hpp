#ifndef INDICES_HPP
#define INDICES_HPP
// macros for flexible axis

// cpu
#define img(i,j)                        img[(i) * img_width + (j)]
#define Ix(i,j)                          Ix[(i) * img_width + (j)]
#define Iy(i,j)                          Iy[(i) * img_width + (j)]
#define grad_mag(i,j)              grad_mag[(i) * img_width + (j)]
#define subpix_idx_map(i,j)  subpix_idx_map[(i) * img_width + (j)]

#define tmp_weak2strong(i,j)                          tmp_weak2strong[(i) * 6 + (j)]
#define subpix_edge_pts_weak(i,j)                subpix_edge_pts_weak[(i) * 6 + (j)]
#define subpix_edge_pts_final(i,j)              subpix_edge_pts_final[(i) * 6 + (j)]
#define subpix_edge_pts_strong(i,j)            subpix_edge_pts_strong[(i) * 6 + (j)]
#define subpix_edge_pts_weak2strong(i,j)  subpix_edge_pts_weak2strong[(i) * 6 + (j)]

#define maskx(i, j)    maskx[(i) * kernel_sz + (j)]
#define masky(i, j)    masky[(i) * kernel_sz + (j)]

// gpu
#define dev_img(i,j)                        dev_img[(i) * img_width + (j)]
#define dev_Ix(i,j)                          dev_Ix[(i) * img_width + (j)]
#define dev_Iy(i,j)                          dev_Iy[(i) * img_width + (j)]
#define dev_grad_mag(i,j)              dev_grad_mag[(i) * img_width + (j)]
#define dev_subpix_idx_map(i,j)  dev_subpix_idx_map[(i) * img_width + (j)]

#define dev_tmp_weak2strong(i,j)                          dev_tmp_weak2strong[(i) * 6 + (j)]
#define dev_subpix_edge_pts_weak(i,j)                dev_subpix_edge_pts_weak[(i) * 6 + (j)]
#define dev_subpix_edge_pts_final(i,j)              dev_subpix_edge_pts_final[(i) * 6 + (j)]
#define dev_subpix_edge_pts_strong(i,j)            dev_subpix_edge_pts_strong[(i) * 6 + (j)]
#define dev_subpix_edge_pts_weak2strong(i,j)  dev_subpix_edge_pts_weak2strong[(i) * 6 + (j)]

#define dev_maskx(i, j)    dev_maskx[(i) * kernel_sz + (j)]
#define dev_masky(i, j)    dev_masky[(i) * kernel_sz + (j)]

#endif // INDICES_HPP