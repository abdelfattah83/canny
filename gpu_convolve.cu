#include"gpu_kernels.hpp"
#include"indices.hpp"

#define cond (bx == 0 && by == 0 && tx == 0 && ty == 0)
#define SIMG_LDA(PIMGX)   ((PIMGX)+0)

template<typename T, int SUBIMGX, int SUBIMGY, int THX, int THY, int KS, int CENT>
__global__
void
gpu_convolve_kernel(
        int img_height, int img_width, T* dev_img,
        T* dev_maskx, T* dev_masky,
        T* dev_Ix, T* dev_Iy,
        T* dev_grad_mag )
{
#define simg(i,j)     simg[(i) * img_lda + (j)]
#define timg(i,j)     timg[(i) * img_lda + (j)]
#define smaskx(i,j) smaskx[(i) * KS + (j)]
#define smasky(i,j) smasky[(i) * KS + (j)]

    extern __shared__ T sdata[];

    const int tid = threadIdx.x;
    const int tx  = tid / THX;
    const int ty  = tid % THX;
    const int bx  = blockIdx.x;
    const int by  = blockIdx.y;

    const int PIMGX = SUBIMGX + (2 * CENT); // padded image x-size
    const int PIMGY = SUBIMGY + (2 * CENT); // padded image y-size


    const int base_gtx = bx * SUBIMGX + tx;
    const int base_gty = by * SUBIMGY + ty;
    const int img_lda  = SIMG_LDA(PIMGX);

    // shared memory ptrs
    T* simg   = (T*)sdata;
    T* smaskx = simg   + (img_lda * PIMGY);
    T* smasky = smaskx + (KS * KS);

    // read gaussian filter
    int i = 0, j = 0;
    #pragma unroll
    for(i = 0;  i < (KS*KS)-(THX*THY); i+= (THX*THY)) {
        smaskx[i + tid] = dev_maskx[i+tid];
        smasky[i + tid] = dev_masky[i+tid];
    }

    if(tid < (KS*KS)-i) {
        smaskx[i + tid] = dev_maskx[i+tid];
        smasky[i + tid] = dev_masky[i+tid];
    }
    __syncthreads();
    // end of reading the gaussian filters

    // read sub-image into shared memory
    #pragma unroll
    for(j = 0; j < PIMGY; j+=THY) {
        #pragma unroll
        for(i = 0; i < PIMGX; i+=THX) {
            int tx_ = base_gtx + i - CENT;
            int ty_ = base_gty + j - CENT;
            simg(i+tx,j+ty) = ( tx_ >= 0 && ty_ >= 0 && tx_ < img_height && ty_ < img_width ) ? dev_img[ tx_ * img_width + ty_ ] : 0.;
        }
    }
    __syncthreads();

    // convolve
    T rIx, rIy, rgrad_mag;
    T* timg = &simg(CENT,CENT);
    #pragma unroll
    for(i = 0; i < SUBIMGX; i+=THX) {
        #pragma unroll
        for(j = 0; j < SUBIMGY; j+=THY) {
            int ii = i + tx;
            int jj = j + ty;

            rIx = 0.;
            rIy = 0.;
            #pragma unroll
            for(int p = -CENT; p <= CENT; p++) {
                #pragma unroll
                for(int q = -CENT; q <= CENT; q++) {
                    rIx += timg(ii+p, jj+q) * smaskx(p+CENT, q+CENT);
                    rIy += timg(ii+p, jj+q) * smasky(p+CENT, q+CENT);
                }
            }

            rgrad_mag = sqrt(rIx*rIx + rIy*rIy);
            int tx_ = base_gtx + i;
            int ty_ = base_gty + j;

            if( tx_ < img_height && ty_ <  img_width) {
                dev_Ix(tx_,ty_) = rIx;
                dev_Iy(tx_,ty_) = rIy;
                dev_grad_mag(tx_,ty_) = rgrad_mag;
            }
        }
    }

    // TODO: write back in a separate loop
}


extern "C"
void gpu_convolve(
        int device_id,
        int h, int w, double* dev_img,
        double* dev_maskx, double* dev_masky,
        double* dev_Ix, double* dev_Iy,
        double* dev_grad_mag )
{
    // kernel parameters
    // TODO: tune
    const int ks   = 17;
    const int cent = (ks-1)/2;
    const int subimgx = 16;
    const int subimgy = 16;
    const int thx     = 16;
    const int thy     = 16;
    // end of kernel parameters

    const int gridx = (h + subimgx - 1) / subimgx;
    const int gridy = (w + subimgy - 1) / subimgy;
    dim3 grid(gridx, gridy, 1);
    dim3 threads(thx*thy,1,1);

    const int simg_lda = SIMG_LDA(subimgx + cent + cent);

    int shmem = 0;
    shmem += sizeof(double) * simg_lda * (subimgx + cent + cent);  // padded sub-image
    shmem += sizeof(double) * (ks * ks); // maskx
    shmem += sizeof(double) * (ks * ks); // masky

    // get max. dynamic shared memory on the GPU
    int nthreads_max, shmem_max = 0;
    cudaDeviceGetAttribute(&nthreads_max, cudaDevAttrMaxThreadsPerBlock, device_id);
    #if CUDA_VERSION >= 9000
    cudaDeviceGetAttribute (&shmem_max, cudaDevAttrMaxSharedMemoryPerBlockOptin, device_id);
    if (shmem <= shmem_max) {
        cudaFuncSetAttribute(gpu_convolve_kernel<double, subimgx, subimgy, thx, thy, ks, cent>, cudaFuncAttributeMaxDynamicSharedMemorySize, shmem);
    }
    #else
    cudaDeviceGetAttribute (&shmem_max, cudaDevAttrMaxSharedMemoryPerBlock, device);
    #endif    // CUDA_VERSION >= 9000

    if ( shmem > shmem_max ) {
        printf("error: kernel %s requires too many threads or too much shared memory\n", __func__);
    }

    void *kernel_args[] = {&h, &w, &dev_img, &dev_maskx, &dev_masky, &dev_Ix, &dev_Iy, &dev_grad_mag};

    cudacheck( cudaLaunchKernel((void*)gpu_convolve_kernel<double, subimgx, subimgy, thx, thy, ks, cent>, grid, threads, kernel_args, shmem, NULL) );
}
